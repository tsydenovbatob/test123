#!/bin/bash
################################################################################
# Author: SYSWEB
#-------------------------------------------------------------------------------
# Ce script va installer Odoo sur votre serveur Ubuntu.
#-------------------------------------------------------------------------------
################################################################################
echo "Lets start"
. /root/config.ini
##
###  WKHTMLTOPDF download links
## === Ubuntu Trusty x64 & x32 === (for other distributions please replace these two links,
## in order to have correct version of wkhtmltopdf installed, for a danger note refer to
## https://github.com/odoo/odoo/wiki/Wkhtmltopdf ):
## https://www.odoo.com/documentation/12.0/setup/install.html#debian-ubuntu

#WKHTMLTOX_X64=https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.trusty_amd64.deb
#WKHTMLTOX_X32=https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.trusty_i386.deb
WKHTMLTOX_X64=https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.xenial_amd64.deb
WKHTMLTOX_X32=https://github.com/wkhtmltopdf/wkhtmltopdf/releases/download/0.12.5/wkhtmltox_0.12.5-1.xenial_i386.deb

#--------------------------------------------------
# Mise à jour du serveur
#--------------------------------------------------

echo -e "\n---- Mise à jour des paquets ----"
 apt update
echo -e "\n---- Installation des software-properties-common ----"
 apt install software-properties-common -y
echo -e "\n---- Mise à jour des repo ----"
 add-apt-repository "deb http://mirrors.kernel.org/ubuntu/ xenial main"
 add-apt-repository universe
echo -e "\n---- Mise à jour du serveur ----"
 apt upgrade -y

#--------------------------------------------------
# Install PostgreSQL Server
#--------------------------------------------------

echo -e "\n---- Installation du serveur PostgreSQL ----"
 apt install postgresql postgresql-server-dev-all -y

echo -e "\n---- Création de l\'utilisateur ODOO PostgreSQL  ----"
 su - postgres -c "createuser -s $OE_USER" 2> /dev/null || true

#--------------------------------------------------
# Installer des dépendances
#--------------------------------------------------
echo -e "\n--- Installation de Python 3 + pip3 --"
 apt install git python3 python3-pip build-essential wget python3-dev python3-venv python3-wheel libxslt-dev libzip-dev libldap2-dev libsasl2-dev python3-setuptools node-less libpng12-0 gdebi -y

echo -e "\n---- Installation des paquets/exigences python ----"
 -H pip3 install -r https://github.com/odoo/odoo/raw/${OE_VERSION}/requirements.txt

echo -e "\n---- Installation de nodeJS NPM et rtlcss pour le support LTR ----"
 apt install nodejs npm -y
 npm install -g rtlcss

#--------------------------------------------------
# Installation de Wkhtmltopdf si nécessaire
#--------------------------------------------------
if [ $INSTALL_WKHTMLTOPDF = "True" ]; then
  echo -e "\n---- Installez wkhtml et placez les raccourcis au bon endroit pour ODOO 13 ----"
  #en choisir une correcte parmi les versions x64 et x32 :
  if [ "`getconf LONG_BIT`" == "64" ];then
      _url=$WKHTMLTOX_X64
  else
      _url=$WKHTMLTOX_X32
  fi
   wget $_url
   gdebi --n `basename $_url`
   ln -s /usr/local/bin/wkhtmltopdf /usr/bin
   ln -s /usr/local/bin/wkhtmltoimage /usr/bin
else
  echo "Wkhtmltopdf n'est pas installé en raison du choix de l'utilisateur!"
fi

echo -e "\n---- Création d'un utilisateur du système ODOO ----"
adduser --system --quiet --shell=/bin/bash --home=$OE_HOME --gecos 'ODOO' --group $OE_USER
#L'utilisateur doit également être ajouté au groupe .
adduser $OE_USER 

echo -e "\n---- Création d'un répertoire des historiques ----"
 mkdir /var/log/$OE_USER
 chown $OE_USER:$OE_USER /var/log/$OE_USER

#--------------------------------------------------
# Installation d'ODOO
#--------------------------------------------------
echo -e "\n==== Installation du serveur ODOO ===="
 git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/odoo $OE_HOME_EXT/

if [ $IS_ENTERPRISE = "True" ]; then
    # Installation d'Odoo Enterprise !
    echo -e "\n--- Création d'un lien symbolique pour le nœud"
     ln -s /usr/bin/nodejs /usr/bin/node
     su $OE_USER -c "mkdir $OE_HOME/enterprise"
     su $OE_USER -c "mkdir $OE_HOME/enterprise/addons"

    GITHUB_RESPONSE=$( git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/enterprise "$OE_HOME/enterprise/addons" 2>&1)
    while [[ $GITHUB_RESPONSE == *"Authentication"* ]]; do
        echo "------------------------! ATTENTION !------------------------------"
        echo "Votre authentification auprès de Github a échoué ! Veuillez réessayer."
        printf "In order to clone and install the Odoo enterprise version you \nneed to be an offical Odoo partner and you need access to\nhttp://github.com/odoo/enterprise.\n"
        echo "ASTUCE : appuyez sur ctrl+c pour arrêter le script."
        echo "-------------------------------------------------------------"
        echo " "
        GITHUB_RESPONSE=$( git clone --depth 1 --branch $OE_VERSION https://www.github.com/odoo/enterprise "$OE_HOME/enterprise/addons" 2>&1)
    done

    echo -e "\n---- Ajout du code d'entreprise ici : $OE_HOME/enterprise/addons ----"
    echo -e "\n---- Installation des bibliothèques spécifiques ODOO Entreprises ----"
     -H pip3 install num2words ofxparse dbfread ebaysdk firebase_admin pyOpenSSL
     npm install -g less
     npm install -g less-plugin-clean-css
fi

echo -e "\n---- Création d'un répertoire pour les modules personnalisés ----"
 su $OE_USER -c "mkdir $OE_HOME/custom"
 su $OE_USER -c "mkdir $OE_HOME/custom/addons"

echo -e "\n---- Définition des autorisations pour le dossier principal ----"
 chown -R $OE_USER:$OE_USER $OE_HOME/*

echo -e "* Création d'un fichier de configuration du serveur"


 touch /etc/${OE_CONFIG}.conf
echo -e "* Création du fichier de configuration du serveur"
 su root -c "printf '[options] \n; Voici le mot de passe de la base de données :\n' >> /etc/${OE_CONFIG}.conf"
if [ $GENERATE_RANDOM_PASSWORD = "True" ]; then
    echo -e "* Génération d'un mot de passe admin aléatoire"
    OE_SUPERADMIN=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -n 1)
fi
 su root -c "printf 'admin_passwd = ${OE_SUPERADMIN}\n' >> /etc/${OE_CONFIG}.conf"
if [ $OE_VERSION > "11.0" ];then
     su root -c "printf 'http_port = ${OE_PORT}\n' >> /etc/${OE_CONFIG}.conf"
else
     su root -c "printf 'xmlrpc_port = ${OE_PORT}\n' >> /etc/${OE_CONFIG}.conf"
fi
 su root -c "printf 'logfile = /var/log/${OE_USER}/${OE_CONFIG}.log\n' >> /etc/${OE_CONFIG}.conf"

if [ $IS_ENTERPRISE = "True" ]; then
     su root -c "printf 'addons_path=${OE_HOME}/enterprise/addons,${OE_HOME_EXT}/addons\n' >> /etc/${OE_CONFIG}.conf"
else
     su root -c "printf 'addons_path=${OE_HOME_EXT}/addons,${OE_HOME}/custom/addons\n' >> /etc/${OE_CONFIG}.conf"
fi
 chown $OE_USER:$OE_USER /etc/${OE_CONFIG}.conf
 chmod 640 /etc/${OE_CONFIG}.conf

echo -e "* Création d'un fichier de lancement"
 su root -c "echo '#!/bin/sh' >> $OE_HOME_EXT/start.sh"
 su root -c "echo ' -u $OE_USER $OE_HOME_EXT/odoo-bin --config=/etc/${OE_CONFIG}.conf' >> $OE_HOME_EXT/start.sh"
 chmod 755 $OE_HOME_EXT/start.sh

#--------------------------------------------------
# Ajout de ODOO en tant que deamon (initscript)
#--------------------------------------------------

echo -e "* Création d'un fichier init"
cat <<EOF > ~/$OE_CONFIG
#!/bin/sh
### DÉBUT INFO INIT
# Propose: $OE_CONFIG
# Required-Start: \$remote_fs \$syslog
# Required-Stop: \$remote_fs \$syslog
# Should-Start: \$network
# Should-Stop: \$network
# Default-Start: 2 3 4 5
# Default-Stop: 0 1 6
# Short-Description: Odoo et SYSWEB est une applications pour les entreprises
# Description: Odoo et SYSWEB
### END INIT INFO
PATH=/sbin:/bin:/usr/sbin:/usr/bin:/usr/local/bin
DAEMON=$OE_HOME_EXT/odoo-bin
NAME=$OE_CONFIG
DESC=$OE_CONFIG
# Specify the user name (Default: odoo).
USER=$OE_USER
# Specify an alternate config file (Default: /etc/openerp-server.conf).
CONFIGFILE="/etc/${OE_CONFIG}.conf"
# pidfile
PIDFILE=/var/run/\${NAME}.pid
# Additional options that are passed to the Daemon.
DAEMON_OPTS="-c \$CONFIGFILE"
[ -x \$DAEMON ] || exit 0
[ -f \$CONFIGFILE ] || exit 0
checkpid() {
[ -f \$PIDFILE ] || return 1
pid=\`cat \$PIDFILE\`
[ -d /proc/\$pid ] && return 0
return 1
}
case "\${1}" in
start)
echo -n "Starting \${DESC}: "
start-stop-daemon --start --quiet --pidfile \$PIDFILE \
--chuid \$USER --background --make-pidfile \
--exec \$DAEMON -- \$DAEMON_OPTS
echo "\${NAME}."
;;
stop)
echo -n "Stopping \${DESC}: "
start-stop-daemon --stop --quiet --pidfile \$PIDFILE \
--oknodo
echo "\${NAME}."
;;
restart|force-reload)
echo -n "Restarting \${DESC}: "
start-stop-daemon --stop --quiet --pidfile \$PIDFILE \
--oknodo
sleep 1
start-stop-daemon --start --quiet --pidfile \$PIDFILE \
--chuid \$USER --background --make-pidfile \
--exec \$DAEMON -- \$DAEMON_OPTS
echo "\${NAME}."
;;
*)
N=/etc/init.d/\$NAME
echo "Usage: \$NAME {start|stop|restart|force-reload}" >&2
exit 1
;;
esac
exit 0
EOF

echo -e "* Fichier de sécurité Init"
 mv ~/$OE_CONFIG /etc/init.d/$OE_CONFIG
 chmod 755 /etc/init.d/$OE_CONFIG
 chown root: /etc/init.d/$OE_CONFIG

echo -e "* Démarrer ODOO au démarrage du serveur"
 update-rc.d $OE_CONFIG defaults

#--------------------------------------------------
# Installez Nginx si nécessaire
#--------------------------------------------------
if [ $INSTALL_NGINX = "True" ]; then
  echo -e "\n---- Installation et mise en place de Nginx ----"
   apt install nginx -y
  cat <<EOF > ~/odoo
  server {
    listen 80;
    listen 443 ssl http2;
    server_name $WEBSITE_NAME;
  #   Fichier de configuration par sysweb

  # Add Headers for odoo proxy mode
  proxy_set_header X-Forwarded-Host \$host;
  proxy_set_header X-Forwarded-For \$proxy_add_x_forwarded_for;
  proxy_set_header X-Forwarded-Proto \$scheme;
  proxy_set_header X-Real-IP \$remote_addr;
  add_header X-Frame-Options "SAMEORIGIN";
  add_header X-XSS-Protection "1; mode=block";
  proxy_set_header X-Client-IP \$remote_addr;
  proxy_set_header HTTP_X_FORWARDED_HOST \$remote_addr;


  #HSTS
  add_header Strict-Transport-Security "max-age=31536000" always;

  #   odoo    log files
  access_log  /var/log/nginx/$OE_USER-access.log;
  error_log       /var/log/nginx/$OE_USER-error.log;

  #   increase    proxy   buffer  size
  proxy_buffers   16  64k;
  proxy_buffer_size   128k;
  proxy_read_timeout 900s;
  proxy_connect_timeout 900s;
  proxy_send_timeout 900s;
  #   force   timeouts    if  the backend dies
  proxy_next_upstream error   timeout invalid_header  http_500    http_502
  http_503;
  types {
  text/less less;
  text/scss scss;
  }
  #   enable  data    compression
  gzip    on;
  gzip_min_length 1100;
  gzip_buffers    4   32k;
  gzip_types  text/css text/less text/plain text/xml application/xml application/json application/javascript application/pdf image/jpeg image/png;
  gzip_vary   on;
  client_header_buffer_size 4k;
  large_client_header_buffers 4 64k;
  client_max_body_size 0;
  location / {
  proxy_pass    http://127.0.0.1:$OE_PORT;
  # by default, do not forward anything
  proxy_redirect off;
  }
  location /longpolling {
  proxy_pass http://127.0.0.1:$LONGPOLLING_PORT;
  }
  location ~* .(js|css|png|jpg|jpeg|gif|ico)$ {
  expires 2d;
  proxy_pass http://127.0.0.1:$OE_PORT;
  add_header Cache-Control "public, no-transform";
  }
  # cache some static data in memory for 60mins.
  location ~ /[a-zA-Z0-9_-]*/static/ {
  proxy_cache_valid 200 302 60m;
  proxy_cache_valid 404      1m;
  proxy_buffering    on;
  expires 864000;
  proxy_pass    http://127.0.0.1:$OE_PORT;
  }
  }
EOF

   mv ~/odoo /etc/nginx/sites-available/
   ln -s /etc/nginx/sites-available/odoo /etc/nginx/sites-enabled/odoo
   rm /etc/nginx/sites-enabled/default
   service nginx reload
   su root -c "printf 'proxy_mode = True\n' >> /etc/${OE_CONFIG}.conf"
  echo "C'est fait ! Le serveur Nginx est en place et fonctionne. La configuration peut être trouvée à l'adresse /etc/nginx/sites-available/odoo"
else
  echo "Nginx n'est pas installé en raison du choix de l'utilisateur!"
fi

#--------------------------------------------------
# Activer ssl avec certbot
#--------------------------------------------------

if [ $INSTALL_NGINX = "True" ] && [ $ENABLE_SSL = "True" ] && [ $ADMIN_EMAIL != "odoo@example.com" ]  && [ $WEBSITE_NAME != "_" ];then
   add-apt-repository ppa:certbot/certbot -y &&  apt update -y
   apt install python-certbot-nginx -y
   certbot --nginx -d $WEBSITE_NAME --noninteractive --agree-tos --email $ADMIN_EMAIL --redirect
   service nginx reload
  echo "SSL/HTTPS est activé!"
else
  echo "Le SSL/HTTPS n'est pas activé en raison du choix de l'utilisateur ou d'une mauvaise configuration !"
fi

echo -e "* Démarrage du service Odoo"
 su root -c "/etc/init.d/$OE_CONFIG start"
echo "-----------------------------------------------------------"
echo "          _____            _____                   _____                   _____                   _____                   _____          ";
echo "         /\    \          |\    \                 /\    \                 /\    \                 /\    \                 /\    \         ";
echo "        /::\    \         |:\____\               /::\    \               /::\____\               /::\    \               /::\    \        ";
echo "       /::::\    \        |::|   |              /::::\    \             /:::/    /              /::::\    \             /::::\    \       ";
echo "      /::::::\    \       |::|   |             /::::::\    \           /:::/   _/___           /::::::\    \           /::::::\    \      ";
echo "     /:::/\:::\    \      |::|   |            /:::/\:::\    \         /:::/   /\    \         /:::/\:::\    \         /:::/\:::\    \     ";
echo "    /:::/__\:::\    \     |::|   |           /:::/__\:::\    \       /:::/   /::\____\       /:::/__\:::\    \       /:::/__\:::\    \    ";
echo "    \:::\   \:::\    \    |::|   |           \:::\   \:::\    \     /:::/   /:::/    /      /::::\   \:::\    \     /::::\   \:::\    \   ";
echo "  ___\:::\   \:::\    \   |::|___|______   ___\:::\   \:::\    \   /:::/   /:::/   _/___   /::::::\   \:::\    \   /::::::\   \:::\    \  ";
echo " /\   \:::\   \:::\    \  /::::::::\    \ /\   \:::\   \:::\    \ /:::/___/:::/   /\    \ /:::/\:::\   \:::\    \ /:::/\:::\   \:::\ ___\ ";
echo "/::\   \:::\   \:::\____\/::::::::::\____/::\   \:::\   \:::\____|:::|   /:::/   /::\____/:::/__\:::\   \:::\____/:::/__\:::\   \:::|    |";
echo "\:::\   \:::\   \::/    /:::/~~~~/~~     \:::\   \:::\   \::/    |:::|__/:::/   /:::/    \:::\   \:::\   \::/    \:::\   \:::\  /:::|____|";
echo " \:::\   \:::\   \/____/:::/    /         \:::\   \:::\   \/____/ \:::\/:::/   /:::/    / \:::\   \:::\   \/____/ \:::\   \:::\/:::/    / ";
echo "  \:::\   \:::\    \  /:::/    /           \:::\   \:::\    \      \::::::/   /:::/    /   \:::\   \:::\    \      \:::\   \::::::/    /  ";
echo "   \:::\   \:::\____\/:::/    /             \:::\   \:::\____\      \::::/___/:::/    /     \:::\   \:::\____\      \:::\   \::::/    /   ";
echo "    \:::\  /:::/    /\::/    /               \:::\  /:::/    /       \:::\__/:::/    /       \:::\   \::/    /       \:::\  /:::/    /    ";
echo "     \:::\/:::/    /  \/____/                 \:::\/:::/    /         \::::::::/    /         \:::\   \/____/         \:::\/:::/    /     ";
echo "      \::::::/    /                            \::::::/    /           \::::::/    /           \:::\    \              \::::::/    /      ";
echo "       \::::/    /                              \::::/    /             \::::/    /             \:::\____\              \::::/    /       ";
echo "        \::/    /                                \::/    /               \::/____/               \::/    /               \::/____/        ";
echo "         \/____/                                  \/____/                 ~~                      \/____/                 ~~              ";
echo "                                                                                                                                          ";
echo "C'est fait ! Le serveur Odoo est en place et fonctionne. Spécifications:"
echo "Port: $OE_PORT"
echo "Utilisateur: $OE_USER"
echo "Utilisateur PostgreSQL: $OE_USER"
echo "Code location: $OE_USER"
echo "Dossier des addons: $OE_USER/$OE_CONFIG/addons/"
echo "Mot de passe superadmin (base de données): $OE_SUPERADMIN"
echo "Démarrer le service Odoo:  service $OE_CONFIG start"
echo "Arrêter le service Odoo:  service $OE_CONFIG stop"
echo "Redémarrer le service Odoo:  service $OE_CONFIG restart"
if [ $INSTALL_NGINX = "True" ]; then
  echo "Fichier de configuration Nginx: /etc/nginx/sites-available/odoo"
fi
echo "-----------------------------------------------------------"
